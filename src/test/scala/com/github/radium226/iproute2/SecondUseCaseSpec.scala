package com.github.radium226.iproute2

import com.github.radium226.system.network._
import com.github.radium226.system.action._
import com.github.radium226.system.network.link._
import com.github.radium226.system.network.route._
import cats.implicits._
import cats.effect._

class SecondUseCaseSpec extends AbstractSpec {

  "We" should "be able to connect to other stuff" in withAction {
    for {
      remotePublicIP           <- Resolver.resolve[IO]("ns23.ovh.com").action
      remotePrivateIP          <- Resolver.resolve[IO]("kimsufi-01.local").action

      dnsIP                     = "8.8.8.8"

      privateLink              <- Links.lookUp[IO]("tun0")
      localPrivateIP           <- privateLink.address[IO]

      publicLink               <- Links.lookUp[IO]("en01")
      defaultRoute             <- publicLink.routes.default[IO]
      _                        <- Routes.delete[IO](defaultRoute)
      _                        <- Routes.add[IO](remotePublicIP, gateway = Some(defaultRoute.gateway), source = None, link = publicLink)
      _                        <- Routes.add[IO](dnsIP, gateway = Some(defaultRoute.gateway), source = None, link = publicLink)
      _                        <- Routes.add[IO](remotePrivateIP, Some(localPrivateIP), privateLink)
    } yield ()
  }

}
