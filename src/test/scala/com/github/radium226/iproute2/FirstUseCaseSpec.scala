package com.github.radium226.iproute2

import com.github.radium226.system.network.namespace._
import com.github.radium226.system.network.link._
import com.github.radium226.system.kernel._

import cats.implicits._
import cats.effect._

import scala.concurrent.duration._

class FirstUseCaseSpec extends AbstractSpec {

  "Everything" should "work in a seamless way" in withIO {
    val actions = for {
      namespace <- Namespaces.add[IO]("firefox")

      lo      <- namespace.links.lookUp[IO]("lo")
      _       <- lo.addresses.add[IO]("127.0.0.1/8")
      _       <- lo.update[IO](State.Up)

      default <- Links.add[IO, VirtualEthernetLink]("veth_default", None, Kind.veth("veth_vpn", Some(namespace)))
      vpn      = default.peerLink

      _       <- vpn.update[IO](State.Up)
      _       <- default.update[IO](State.Up)

      _       <- vpn.addresses.add[IO]("10.10.10.11/31")
      _       <- default.addresses.add[IO]("10.10.10.10/31")

      _       <- vpn.routes.add[IO]("10.10.10.10", None)
      _       <- Parameters.set[IO]("net.ipv4.ip_forward", "1")
      _       <- IPTables.execute[IO]("iptables", "--table", "nat", "--append", "POSTROUTING", "--jump", "MASQUERADE", "--source", "10.10.10.11/31")
      _       <- namespace.dnsServers.add[IO]("8.8.8.8")
    } yield ()

    actions.use({ _ => IO.sleep(1 minute) })
  }

}
