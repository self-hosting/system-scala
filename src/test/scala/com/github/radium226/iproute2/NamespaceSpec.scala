package com.github.radium226.iproute2

import cats._
import cats.data._
import cats.implicits._
import cats.effect._

import com.github.radium226.system.network.namespace._
import com.github.radium226.system.action._

class NamespaceSpec extends AbstractSpec {

  "Namespaces" should "be listed" in withIO {
    for {
      _ <- Namespaces.add[IO]("toto").transient.use({ _ =>
        for {
          namespaces <- Namespaces.list[IO].persistent

          _ = println(namespaces)
        } yield ()
      })
    } yield ()
  }

}
