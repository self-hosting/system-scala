package com.github.radium226.iproute2

import cats.implicits._
import cats.effect._
import com.github.radium226.system.execute._
import fs2._
import fs2.concurrent._

class ExecuteSpec extends AbstractSpec {

  "Executor" should "be able to execute" in withIO {
    val sudoExecutor = Executor[IO](sudo = Some(true))
    for {
      stdout   <- Executor.execute[IO]("sudo", "ip", "link", "show").foreground[String](Keep.stdout)
      exitCode <- Executor.execute[IO]("false").foreground[ExitCode](Keep.exitCode)
      root     <- sudoExecutor.execute("id", "-nu").foreground[String](Keep.stdout)
      _ = println((stdout, exitCode, root))
    } yield ()
  }

  "Executor" should "be able to stream lines" in withIO {
    val lines = Executor.execute[IO]("bash", "-c", "for i in $( seq 1 10 ); do echo 'StdOut' ; sleep 1 ; echo 'StdErr' >&2 ; sleep 1 ; done").stream

    lines.evalTap({ line => IO(println(line)) }).compile.drain
  }

  def lines[F[_]](implicit F: Concurrent[F]): Pipe[F, Output, Line] = { stream =>
    stream
      .interruptWhen(stream.collect({
        case End =>
          true

        case _ =>
          false
      }))
      .collect({
        case line @ Line(_, _, _) =>
          line
      })
  }

  "Executor" should "be able to stream lines with multiple subscribers" in withIO {
    for {
      topic  <- Executor.execute[IO]("bash", "-c", "for i in $( seq 1 10 ); do sleep 1 ; echo StdOut ${i} ; sleep 1 ; echo StdErr  ${i} >&2 ; done").topic

      fiber1 <- topic.subscribe(5).through(lines[IO])
        .evalTap({ line =>
          IO.delay(println(s"First: ${line.content} / ${Thread.currentThread().getId}"))
        })
        .compile
        .drain
        .start

      fiber2  <- topic.subscribe(5).through(lines[IO])
        .evalTap({ line =>
          IO.delay(println(s"Second: ${line.content} / ${Thread.currentThread().getId}"))
        })
        .compile
        .drain
        .start

      _       <- fiber1.join
      _       <- fiber2.join
    } yield ()
  }

}
