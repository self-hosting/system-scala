package com.github.radium226.iproute2

import com.github.radium226.system.network._
import com.github.radium226.system.action._
import com.github.radium226.system.network.link._
import com.github.radium226.system.network.route._
import com.github.radium226.tinc._
import cats.implicits._
import cats.effect._
import com.github.radium226.system.network.namespace._


class ThirdUseCase extends AbstractSpec {

  "We" should "be able to make all this shit work together" in withIO {
    val actions = for {
      remotePublicIP           <- Network("vpn").nodeInfo[IO]("kimsufi_01").action
      remotePrivateIP          <- Resolver.resolve[IO]("kimsufi-01.local").action

      namespace                <- Namespaces.add[IO]("rigolux")

    } yield (remotePublicIP, remotePrivateIP)

    actions.transient.use { case (remotePublicIP, remotePrivateIP) =>
      IO.delay(println(s"remotePublicIP=${remotePublicIP} / remotePrivateIP=${remotePrivateIP}"))
    }
  }

}
