package com.github.radium226.iproute2

import cats.effect._
import com.github.radium226.system.action._
import com.github.radium226.system.execute.Executor
import org.scalatest.FlatSpec

import scala.concurrent.ExecutionContext

abstract class AbstractSpec extends FlatSpec {

  implicit val timer: Timer[IO] = IO.timer(ExecutionContext.Implicits.global)

  implicit val contextShift: ContextShift[IO] = IO.contextShift(ExecutionContext.Implicits.global)

  def withIO(f: => IO[Any]): Any = {
    val io = f

    io.unsafeRunSync()
  }

  def withAction(f: => Action[IO, Any]): Any = {
    withIO(f.transient.use({ _ => IO(println("Done! ")) }))
  }

}
