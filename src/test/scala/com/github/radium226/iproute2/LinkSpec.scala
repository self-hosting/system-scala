package com.github.radium226.iproute2

import cats.implicits._
import cats.effect._
import com.github.radium226.system.network.link._
import com.github.radium226.system.network.route._

class LinkSpec extends AbstractSpec {

  "Links" should "be listed" in withAction {
    for {
      links <- Links.list[IO]
      _ = links.foreach(println)
    } yield links
  }

  "Link" should "have an address, a state" in withAction {
    for {
      links <- Links.list[IO]
      link = links.find(_.name == "lo").get
      address <- link.address[IO]
      state <- link.state[IO]
      _ = println(address)
      _ = println(state)
    } yield address
  }

  "A given link" should "be able to list routes" in withAction {
    for {
      links <- Links.list[IO]
      //routes <- links.flatTraverse[IO, Route](_.routes.list[IO])
      //_ = println(routes)
    } yield ()// routes
  }

}
