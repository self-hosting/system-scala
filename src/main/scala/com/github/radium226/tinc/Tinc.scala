package com.github.radium226.tinc

import cats.implicits._
import cats.effect._

import com.github.radium226.system.network._
import com.github.radium226.system.execute._

case class Address(ip: IP, port: Port)

case class NodeInfo(name: NodeName, address: Address)

case class Network(name: NetworkName) {

  def nodeInfo[F[_]](nodeName: NodeName)(implicit F: Sync[F]): F[NodeInfo] = {
    val regex = "^Address: +([0-9\\.]+) port ([0-9]+)$".r("ip", "port")
    Executor(sudo = Some(true))
      .execute("tinc", "-n", name, "info", nodeName)
      .foreground[Stdout](Keep.stdout)
      .flatMap(_.split("\n")
        .collectFirst({
          case regex(ip, port) =>
            Address(ip, port.toInt)
        })
        .map(F.pure)
        .getOrElse(F.raiseError[Address](new Exception(s"Unable to find ${nodeName} node info! "))))
      .map({ address =>
        NodeInfo(nodeName, address)
      })
  }

}
