package com.github.radium226.system

import java.time.LocalDateTime

package object execute {

  type Argument = String

  type User = String

  type Stdout = String

  sealed trait Source

  case object StdErr extends Source

  case object StdOut extends Source

  sealed trait Output

  case class Line(number: Long, content: String, source: Source) extends Output

  case object Begin extends Output

  case object End extends Output

}
