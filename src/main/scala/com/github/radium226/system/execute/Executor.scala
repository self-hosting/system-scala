package com.github.radium226.system.execute

import java.nio.file.Path

import cats.effect._
import com.github.radium226.system.network.namespace._

class Executor[F[_]](val user: Option[User] = None, val networkNamespace: Option[Namespace] = None, val sudo: Option[Boolean] = None, val workingFolderPath: Option[Path] = None) {

  def execute(arguments: Argument*): Execute[F] = {
    Execute(this, List(arguments: _*))
  }

  def java(className: String, classPath: Option[List[Path]] = None, arguments: List[Argument] = List.empty[Argument]): Execute[F] = {
    execute(List("java", "--add-opens",
        "java.base/jdk.internal.reflect=ALL-UNNAMED",
    "--add-opens",
    "java.base/jdk.internal.loader=ALL-UNNAMED",
    "--add-opens",
    "jdk.zipfs/jdk.nio.zipfs=ALL-UNNAMED",
    "--add-opens",
    "java.base/java.lang=ALL-UNNAMED", "-cp", classPath.map(_.map(_.toString).mkString(":")).getOrElse(System.getProperty("java.class.path")), className) ++ arguments: _*)
  }

  def kill(process: Process)(implicit F: Sync[F]): Execute[F] = {
    execute("kill", "-TERM", "--", s"-${process.pid()}")
  }

  def withWorkingFolder(workingFolderPath: Path): Executor[F] = {
    new Executor[F](user, networkNamespace, sudo, Some(workingFolderPath))
  }

}

object Executor {

  def javaArguments(className: String, classPath: Option[List[Path]] = None, arguments: List[Argument] = List.empty[Argument]): List[Argument] = {
    List("java", "-cp", classPath.map(_.map(_.toString).mkString(":")).getOrElse(System.getProperty("java.class.path")), className) ++ arguments
  }

  def apply[F[_]](user: Option[User] = None, networkNamespace: Option[Namespace] = None, sudo: Option[Boolean] = None, workingFolderPath: Option[Path] = None): Executor[F] = {
    new Executor[F](user, networkNamespace, sudo, workingFolderPath)
  }

  def apply[F[_]]: Executor[F] = {
    new Executor[F]()
  }

  def execute[F[_]](arguments: Argument*): Execute[F] = {
    val executor = Executor[F]()
    executor.execute(arguments: _*)
  }

}
