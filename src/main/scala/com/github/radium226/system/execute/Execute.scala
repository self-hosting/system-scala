package com.github.radium226.system.execute

import cats._
import cats.data._
import cats.effect._
import cats.implicits._
import fs2._
import fs2.concurrent._

case class Execute[F[_]](executor: Executor[F], command: List[Argument]) {

  def foreground(implicit F: Sync[F]): F[ExitCode] = {
    foreground(Keep.exitCode)
  }

  def foreground[O](keep: Keep[O])(implicit F: Sync[F]): F[O] = {
    for {
      process <- background[O](keep)
      _ <- F.delay(keep.handleProcess(process))
      exitValue <- F.delay(process.waitFor())
      //_ <- if (exitValue == 0) F.unit else F.raiseError[Unit](new Exception(s"Exit value was not 0 (exitValue=${exitValue})! "))
    } yield keep.handleOutcome(exitValue)
  }

  def stream(implicit F: Concurrent[F], contextShift: ContextShift[F]): Stream[F, Line] = {
    (for {
      blocker <- Stream.resource[F, Blocker](Blocker[F])
      process <- Stream.eval[F, Process] (F.delay( {
        val networkNamespaceCommand = executor.networkNamespace
            .map({ networkNamespace =>
              List(
                "sudo",
                "-E",
                "ip",
                "netns",
                "exec",
                networkNamespace.name,
                "sudo",
                "-E",
                "-u",
                System.getProperty("user.name"))
            })
            .getOrElse(List.empty[Argument])

        val userCommand = executor.user
            .map({ user =>
              List("sudo", "-E", "-u", user)
            })
            .getOrElse(List.empty[Argument])

        val sudoCommand = executor.sudo
            .map({
              case true =>
                List("sudo")

              case false =>
                List.empty[Argument]
            })
            .getOrElse(List.empty[Argument])

        val completeCommand: List[Argument] = List("setsid") ++ networkNamespaceCommand ++ userCommand ++ sudoCommand ++ command
        println(s"command=${completeCommand}")

        var processBuilder = new ProcessBuilder()
            .command(completeCommand: _*)
            .redirectOutput(ProcessBuilder.Redirect.PIPE)
            .redirectError(ProcessBuilder.Redirect.PIPE)

        processBuilder = executor.workingFolderPath
            .map({ workingFolderPath =>
              processBuilder.directory(workingFolderPath.toFile)
            })
            .getOrElse(processBuilder)

        val process = processBuilder.start()
        println("Process started! ")
        process
      }))
    } yield (blocker, process)).flatMap({ case (blocker, process) =>
      val stdErr = fs2.io.readInputStream[F](F.delay(process.getErrorStream), 1, blocker)
        .through(fs2.text.utf8Decode)
        .through(fs2.text.lines[F])
        .map({ content =>
          (content, StdErr)
        })
      val stdOut = fs2.io.readInputStream[F](F.delay(process.getInputStream), 1, blocker)
        .through(fs2.text.utf8Decode)
        .through(fs2.text.lines[F])
        .map({ content =>
          (content, StdOut)
        })

      stdOut.merge(stdErr).zipWithIndex.map({ case ((content, source), number) =>
          Line(number, content, source)
      })
    })
  }

  def topic(implicit F: Concurrent[F], contextShift: ContextShift[F]): F[Topic[F, Output]] = {
    for {
      topic <- Topic[F, Output](Begin)
      _     <- F.start((stream ++ Stream.emit[F, Output](End)).through(topic.publish).compile.drain)

    } yield topic
  }

  def fiber[O](keep: Keep[O])(implicit F: Concurrent[F]): F[Fiber[F, O]] = {
    F.start(foreground(keep))
  }

  def background[O](keep: Keep[O])(implicit F: Sync[F]): F[Process] = F.delay {
    val networkNamespaceCommand = executor.networkNamespace
      .map({ networkNamespace =>
        List(
          "sudo", "-E", "ip", "netns", "exec", networkNamespace.name, "sudo", "-E", "-u", System.getProperty("user.name"))
      })
      .getOrElse(List.empty[Argument])

    val userCommand = executor.user
      .map({ user =>
        List("sudo", "-E", "-u", user)
      })
      .getOrElse(List.empty[Argument])

    val sudoCommand = executor.sudo
      .map({
        case true =>
          List("sudo")

        case false =>
          List.empty[Argument]
      })
      .getOrElse(List.empty[Argument])

    val completeCommand: List[Argument] = List("setsid") ++ networkNamespaceCommand ++ userCommand ++ sudoCommand ++ command
    println(s"command=${completeCommand} / workingFolderPath=${executor.workingFolderPath}")

    var processBuilder = new ProcessBuilder()
        .command(completeCommand: _*)
        .redirectError(ProcessBuilder.Redirect.INHERIT)

    processBuilder = executor.workingFolderPath
      .map({ workingFolderPath =>
        processBuilder.directory(workingFolderPath.toFile)
      })
      .getOrElse(processBuilder)

    keep.handleProcessBuilder(processBuilder)

    val process = processBuilder.start()
    process
  }

  def resource(implicit F: Sync[F]): Resource[F, Unit] = {
    Resource.make(background[Unit](Keep.unit))({ process => executor.kill(process).foreground[Unit](Keep.unit)}).void
  }

}
