package com.github.radium226.system.execute

import java.io.{BufferedReader, InputStreamReader}
import java.util.StringJoiner

import cats.effect.ExitCode

trait Keep[T] {

  def handleProcessBuilder(processBuilder: ProcessBuilder): Unit

  def handleProcess(process: Process): Unit

  def handleOutcome(code: Int): T

}

object Keep {

  def exitCode: Keep[ExitCode] = new Keep[ExitCode] {

    override def handleProcessBuilder(processBuilder: ProcessBuilder): Unit = {
      processBuilder.redirectOutput(ProcessBuilder.Redirect.INHERIT)
    }

    override def handleProcess(process: Process): Unit = {

    }

    override def handleOutcome(code: Int): ExitCode = code match {
      case 0 =>
        ExitCode.Success

      case _ =>
        ExitCode.Error
    }

  }

  def stdout: Keep[String] = new Keep[String] {

    val stringJoiner = new StringJoiner(System.getProperty("line.separator"))

    override def handleProcessBuilder(processBuilder: ProcessBuilder): Unit = {
      processBuilder.redirectOutput(ProcessBuilder.Redirect.PIPE)
    }

    override def handleProcess(process: Process): Unit = {
      val reader = new BufferedReader(new InputStreamReader(process.getInputStream))
      reader
        .lines
        .iterator
        .forEachRemaining({ line =>
          stringJoiner.add(line)
        })
    }

    override def handleOutcome(code: Int): String = {
      stringJoiner.toString
    }

  }

  def unit: Keep[Unit] = new Keep[Unit] {

    override def handleProcessBuilder(processBuilder: ProcessBuilder): Unit = {
      processBuilder.redirectOutput(ProcessBuilder.Redirect.INHERIT)
    }

    override def handleProcess(process: Process): Unit = {

    }

    override def handleOutcome(code: Int): Unit = {

    }

  }

}
