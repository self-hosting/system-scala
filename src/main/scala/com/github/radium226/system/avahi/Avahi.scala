package com.github.radium226.system.avahi

import cats.implicits._
import cats.effect._
import com.github.radium226.system.network._
import com.github.radium226.system.execute._

case object Avahi extends Resolver {

  def resolve[F[_]](hostName: HostName)(implicit F: Sync[F]): F[IP] = {
    val regex = "^[a-z0-9\\.-]+\t+([0-9\\.]+)$".r("ip")
    val execute = Executor.execute[F]("avahi-resolve", "-n", "-4", hostName).foreground[Stdout](Keep.stdout)
    execute.flatMap(_.split("\n")
      .collectFirst({
        case regex(ip) =>
          ip
      })
      .map(F.pure)
      .getOrElse(F.raiseError[IP](new Exception(s"Unable to resolve ${hostName} host with Avahi! "))))
  }

}
