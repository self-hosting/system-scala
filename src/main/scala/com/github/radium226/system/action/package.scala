package com.github.radium226.system

import cats._
import cats.data._
import cats.implicits._
import cats.effect._

package object action {

  type Action[F[_], A] = Resource[F, A]

  implicit class ActionOps[F[_], A](action: Action[F, A]) {

    def persistent(implicit F: Sync[F]): F[A] = action.allocated.map(_._1)

    def transient(implicit F: Sync[F]): Resource[F, A] = action

  }

  object Action {

    def make[F[_], A](execute: F[A])(rollback: => Action[F, _])(implicit F: Sync[F]): Action[F, A] = {
      Resource.make(execute)({ _ => rollback.allocated.map(_._2) })
    }

    def lift[F[_], A](execute: F[A])(implicit F: Sync[F]): Action[F, A] = {
      Resource.liftF(execute)
    }

    def pure[F[_], A](a: A)(implicit F: Sync[F]): Action[F, A] = {
      lift(F.pure(a))
    }

    def identity[F[_]](implicit F: Sync[F]): Action[F, Unit] = {
      Resource.liftF(F.unit)
    }

    def raiseError[F[_], A](throwable: Throwable)(implicit F: Sync[F]): Action[F, A] = {
      Action.lift(F.raiseError(throwable))
    }

  }

  implicit class FImplicits[F[_], A](fa: F[A]) {

    def action(implicit F: Sync[F]): Action[F, A] = {
      Action.lift(fa)
    }

  }

}
