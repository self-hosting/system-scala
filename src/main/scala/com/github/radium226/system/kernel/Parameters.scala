package com.github.radium226.system.kernel

import cats.implicits._
import cats.effect._

import com.github.radium226.system.action._
import com.github.radium226.system.execute._

object Parameters {

  def set[F[_]](parameterName: ParameterName, newParameterValue: ParameterValue)(implicit F: Sync[F]): Action[F, Unit] = {
    val execute = Executor(sudo = Some(true))
      .execute("sysctl", s"${parameterName}=${newParameterValue}")
      .foreground[Unit](Keep.unit)

    for {
      oldParameterValue <- get(parameterName)
      _ <- if (oldParameterValue == newParameterValue) Action.identity
           else Action.make(execute)(set(parameterName, oldParameterValue))
    } yield ()
  }

  def get[F[_]](parameterName: ParameterName)(implicit F: Sync[F]): Action[F, ParameterValue] = {
    Executor(sudo = Some(true))
      .execute("sysctl", "-n", parameterName)
      .foreground[String](Keep.stdout)
      .action
  }

}
