package com.github.radium226.system.kernel

import cats.effect._

import com.github.radium226.system.execute._
import com.github.radium226.system.action._

object IPTables {

  @deprecated("You should use another method (which probably does not exists...")
  def execute[F[_]](arguments: Argument*)(implicit F: Sync[F]): Action[F, Unit] = {
    Executor(sudo = Some(true))
      .execute(arguments: _*)
      .foreground[Unit](Keep.unit)
      .action
  }

}
