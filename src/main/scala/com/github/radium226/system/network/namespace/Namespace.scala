package com.github.radium226.system.network.namespace

import cats.effect._
import cats.implicits._
import com.github.radium226.system.action._
import com.github.radium226.system.network.link._
import com.github.radium226.system.network.dns
import com.github.radium226.system.execute.{Executor, Keep}

case class Namespace(name: String) {

  def links: Links = {
    new Links(Some(this))
  }

  def dnsServers: dns.Servers = {
    new dns.Servers(Some(this))
  }

}

object Namespaces {

  def add[F[_]](namespace: Namespace)(implicit F: Sync[F]): Action[F, Namespace] = {
    val arguments = List("ip", "netns", "add", namespace.name)
    val execute = Executor(sudo = Some(true), networkNamespace = None).execute(arguments: _*).foreground.as(namespace)
    Action.make(execute)(delete(namespace))
  }

  def add[F[_]](name: Name)(implicit F: Sync[F]): Action[F, Namespace] = {
    add(Namespace(name))
  }

  def delete[F[_]](namespace: Namespace)(implicit F: Sync[F]): Action[F, Namespace] = {
    val arguments = List("ip", "netns", "delete", namespace.name)
    val execute = Executor(sudo = Some(true), networkNamespace = None).execute(arguments: _*).foreground.as(namespace)
    Action.make(execute)(add(namespace))
  }

  def delete[F[_]](name: Name)(implicit F: Sync[F]): Action[F, Namespace] = {
    delete(Namespace(name))
  }

  def list[F[_]](implicit F: Sync[F]): Action[F, List[Namespace]] = {
    val namespaces = for {
      stdout <- Executor(sudo = Some(true), networkNamespace = None).execute("ip", "netns", "list").foreground(Keep.stdout)
      names = stdout.split("\n").toList
      namespaces = names.map(Namespace.apply)
    } yield namespaces

    namespaces.action
  }

  def lookUp[F[_]](name: Name)(implicit F: Sync[F]): Action[F, Namespace] = {
    list[F]
      .flatMap({ namespaces =>
        namespaces
          .find(_.name == name)
          .map({ namespace => F.pure(namespace) })
          .getOrElse(F.raiseError[Namespace](new Exception(s"Unable to find the ${name} namespace! ")))
          .action
    })
  }

}
