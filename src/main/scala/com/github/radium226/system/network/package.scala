package com.github.radium226.system

import cats.data._
import cats.implicits._
import cats.effect._

import org.http4s._
import org.http4s.Uri.uri
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.dsl._
import org.http4s.dsl.io._

import com.github.radium226.system.network._

import scala.concurrent.duration.Duration

import scala.concurrent.ExecutionContext.Implicits.{ global => globalExecutionContext}


package object network {

  implicit def convertHostNameToHost(hostName: HostName): Host = {
    Host(hostName)
  }

  type CIDR = String

  type IP = String

  type URL = String

  object IP {

    def loopback[F[_]](implicit F: Sync[F]): F[IP] = F.pure("127.0.0.1")

    val URI = uri("https://ipinfo.io/ip")

    /*implicit def ipEntityDecoder[F[_]](implicit F: Sync[F]): EntityDecoder[F, IP] = EntityDecoder.decodeBy(MediaType.text.plain) { message =>
      EitherT {
        message.as[String].map({ value => value.trim.asRight[DecodeFailure] })
      }
    }*/

    def global[F[_]](implicit F: ConcurrentEffect[F]): F[IP] = {
      BlazeClientBuilder[F](globalExecutionContext)
        .resource.use[IP](_.expect[IP](Request[F](method = GET, uri = URI)).map(_.trim))
    }

  }

  type HostName = String

  type Port = Int

  type Table = String

}
