package com.github.radium226.system.network

import cats.implicits._
import cats.effect._

import com.github.radium226.system.avahi.Avahi

trait Resolver {

  def resolve[F[_]](hostName: HostName)(implicit F: Sync[F]): F[IP]

}

object Resolver extends Resolver {

  def resolve[F[_]](hostName: HostName)(implicit F: Sync[F]): F[IP] = {
    List(Host, Avahi)
      .traverse(_.resolve(hostName).attempt)
        .map({t => println(t) ; t})
      .map(_.collectFirst({
        case Right(ip) =>
          ip
      }))
      .flatMap({
        case Some(ip) =>
          F.pure(ip)

        case None =>
          F.raiseError(new Exception(s"Unable to resolve ${hostName}! "))
      })
  }

}
