package com.github.radium226.system.network

import com.github.radium226.system.execute._

import cats.implicits._
import cats.effect._


case class Host(name: HostName)

object Host extends Resolver {

  def resolve[F[_]](hostName: HostName)(implicit F: Sync[F]): F[IP] = {
    resolve(Host(hostName))
  }

  // FIXME
  def resolve[F[_]](host: Host)(implicit F: Sync[F]): F[IP] = {
    Executor.execute("sh", "-c", s"dig +short -4 ${host.name} | awk '{ print $$1 }' | head -n1").foreground[IP](Keep.stdout).flatMap({
      case "" =>
        F.raiseError[IP](new Exception(s"Unable to resolve ${host.name} host! "))
      case ip =>
        F.pure(ip)
    })
  }

}
