package com.github.radium226.system.network.link

import cats.effect._
import cats.implicits._

import com.github.radium226.system.execute._
import com.github.radium226.system.action._

import com.github.radium226.system.network._
import com.github.radium226.system.network.route._
import com.github.radium226.system.network.namespace._

sealed trait State {

  def rollback: State

}

object State {

  case object Up extends State { def rollback = Down }

  case object Down extends State { def rollback = Up }

  case object Unknown extends State { def rollback = Unknown }

}

abstract class Link[T](val name: Name, val namespace: Option[Namespace], val kind: Kind[T]) {
  self: Link[T] with T =>

  def address[F[_]](implicit F: Sync[F]): Action[F, IP] = {
    val ip = for {
      stdout <- Executor[F](sudo = Some(true), networkNamespace = namespace).execute("ip", "address", "show", name).foreground[String](Keep.stdout)
      ip = "inet ([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3})/".r("ip")
          .findFirstMatchIn(stdout)
          .getOrElse(throw new Exception(s"Unable to find address of ${name} link! "))
          .group("ip")
    } yield ip

    ip.action
  }

  def state[F[_]](implicit F: Sync[F]): Action[F, State] = {
    val state = for {
      stdout <- Executor[F](sudo = Some(true), networkNamespace = namespace).execute("ip", "link", "show", name).foreground[String](Keep.stdout)
      state <- "state (UP|DOWN|UNKNOWN)".r("state")
          .findFirstMatchIn(stdout)
          .map(_.group("state"))
          .collect[F[State]]({
            case "UP" =>
              F.pure(State.Up)

            case "DOWN" =>
              F.pure(State.Down)

            case "UNKNOWN" =>
              F.pure(State.Unknown)
          })
          .getOrElse(F.raiseError[State](new Exception(s"Unable to find state of ${name}")))
    } yield state

    state.action
  }

  def update[F[_]](state: State)(implicit F: Sync[F]): Action[F, Unit] = { // FIXME: We should return an Action
    val execute = for {
      stateAsString <- state match {
        case State.Up =>
          F.pure("up")

        case State.Down =>
          F.pure("down")

        case _ =>
          F.raiseError[String](new Exception(s"Unable to set ${state} state! "))
      }
      stdout <- Executor[F](sudo = Some(true), networkNamespace = namespace).execute("ip", "link", "set", name, stateAsString).foreground[Unit](Keep.unit)
    } yield ()

    Action.make(execute)(update(state.rollback))
  }

  def addresses: Addresses = {
    new Addresses(this)
  }

  def routes: Routes = {
    new Routes(this)
  }

}

case class DummyLink(override val name: Name, override val kind: Kind[DummyLink], override val namespace: Option[Namespace]) extends Link[DummyLink](name, namespace, kind)

case class VirtualEthernetLink(override val name: Name, peerLinkName: Name, override val namespace: Option[Namespace], peerLinkNamespace: Option[Namespace], override val kind: Kind[VirtualEthernetLink]) extends Link[VirtualEthernetLink](name, namespace, kind) {

  def peerLink: VirtualEthernetLink = VirtualEthernetLink(peerLinkName, name, peerLinkNamespace, namespace, Kind.veth(name, namespace))

}

case class UnknownLink(override val name: Name, override val kind: Kind[UnknownLink], override val namespace: Option[Namespace]) extends Link[UnknownLink](name, namespace, kind)

object Link {

  def dummy(name: Name, namespace: Option[Namespace]): DummyLink = {
    DummyLink(name, Kind.dummy, namespace)
  }

  def veth(name: Name, peerLinkName: Name, namespace: Option[Namespace], peerLinkNamespace: Option[Namespace]): VirtualEthernetLink = {
    VirtualEthernetLink(name, peerLinkName, namespace, peerLinkNamespace, Kind.veth(peerLinkName, peerLinkNamespace))
  }

}

class Links(namespace: Option[Namespace]) {

  def list[F[_]](implicit F: Sync[F]): Action[F, List[Link[_]]] =  {
    val links = for {
      linkNames <- Executor[F](sudo = Some(true), networkNamespace = namespace).execute("ls", "-1", "/sys/class/net").foreground(Keep.stdout)
      links = linkNames.split("\n").toList
        .map({ linkName =>
          Link.dummy(linkName, namespace)
        })
        .map(_.asInstanceOf[Link[_]]) //FIXME: Is it necessary?
    } yield links

    links.action
  }

  def lookUp[F[_], T <: Link[T]](name: Name, kind: Kind[T])(implicit F: Sync[F]): Action[F, T] = {
    lookUp(name)
      .flatMap({ link =>
        if (link.kind == kind) Action.pure(link.asInstanceOf[T]) // FIXME: Is it necessary?
        else Action.raiseError(new Exception(s"There is no ${name} link of ${kind} kind! "))
      })
  }

  def lookUp[F[_]](name: Name)(implicit F: Sync[F]): Action[F, Link[_]] = {
    list[F]
      .flatMap({ links =>
        links
          .find(_.name == name)
          .map({ link =>
            Action.pure[F, Link[_]](link)
          })
          .getOrElse(Action.raiseError[F, Link[_]](new Exception(s"There is no ${name} link! ")))
      })
  }

  def add[F[_], T <: Link[T]](link: T)(implicit F: Sync[F]): Action[F, T] = {
    val execute = link.kind.execute(link.name, link.namespace)
    Action.make(execute.as(link))(delete(link))
  }

  def add[F[_], T <: Link[T]](name: Name, namespace: Option[Namespace], kind: Kind[T])(implicit F: Sync[F]): Action[F, T] = {
    add[F, T](kind.apply(name, namespace))
  }

  def delete[F[_], T <: Link[T]](link: T)(implicit F: Sync[F]): Action[F, Unit] = {
    val execute = for {
      _ <- Executor[F](sudo = Some(true), networkNamespace = namespace)
          .execute("ip", "link", "del", link.name)
          .foreground[Unit](Keep.unit)
    } yield ()
    Action.make(execute)(add[F, T](link))
  }

}

case object Links extends Links(None) {

  def in(namespace: Option[Namespace]): Links = {
    new Links(namespace)
  }

  def in(namespace: Namespace): Links = {
    new Links(Some(namespace))
  }

}