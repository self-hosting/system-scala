package com.github.radium226.system.network.link

import cats.implicits._
import cats.effect._

import com.github.radium226.system.network.namespace._
import com.github.radium226.system.execute._

sealed trait Kind[T] {

  def execute[F[_]](linkName: Name, linkNamespace: Option[Namespace])(implicit F: Sync[F]): F[Unit]

  def apply(linkName: Name, linkNamespace: Option[Namespace]): T

}

case object DummyKind extends Kind[DummyLink] {

  override def execute[F[_]](linkName: Name, linkNamespace: Option[Namespace])(implicit F: Sync[F]): F[Unit] = {
    Executor(sudo = Some(true), networkNamespace = linkNamespace)
      .execute("ip", "link", "add", linkName, "type", "dummy")
      .foreground[Unit](Keep.unit)
  }

  override def apply(linkName: Name, linkNamespace: Option[Namespace]): DummyLink = {
    DummyLink(linkName, this, linkNamespace)
  }

}

case class VirtualEthernetKind(peerLinkName: Name, peerLinkNamespace: Option[Namespace]) extends Kind[VirtualEthernetLink] {
  self: VirtualEthernetKind =>

  override def execute[F[_]](linkName: Name, linkNamespace: Option[Namespace])(implicit F: Sync[F]): F[Unit] = {
    val executor = Executor[F](sudo = Some(true), networkNamespace = linkNamespace)
    for {
      _ <- executor.execute("ip", "link", "add", linkName, "type", "veth", "peer", peerLinkName).foreground[Unit](Keep.unit)
      _ <- peerLinkNamespace
          .map({ peerLinkNamespace => executor.execute("ip", "link", "set", peerLinkName, "netns", peerLinkNamespace.name).foreground[Unit](Keep.unit) })
          .getOrElse(F.unit)
    } yield ()
  }

  override def apply(linkName: Name, linkNamespace: Option[Namespace]): VirtualEthernetLink = {
    VirtualEthernetLink(linkName, peerLinkName, linkNamespace, peerLinkNamespace, this)
  }

}


object Kind {

  def dummy: Kind[DummyLink] = DummyKind

  def veth(peerLinkName: Name, peerLinkNamespace: Option[Namespace]): Kind[VirtualEthernetLink] = VirtualEthernetKind(peerLinkName, peerLinkNamespace)

}
