package com.github.radium226.system.network.route


import cats._
import cats.data._
import cats.implicits._
import cats.effect._

import com.github.radium226.system.execute._
import com.github.radium226.system.action._

import com.github.radium226.system.network._
import com.github.radium226.system.network.link._


abstract class Route(val source: Option[IP], val link: Link[_])

case class SpecificRoute(val range: CIDR, val gateway: Option[IP], override val source: Option[IP], override val link: Link[_]) extends Route(source, link)

case class DefaultRoute(val gateway: IP, override val source: Option[IP], override val link: Link[_]) extends Route(source, link)

object Route {

  def specific(range: CIDR, gateway: Option[IP], source: Option[IP], link: Link[_]): Route = {
    SpecificRoute(range, gateway, source, link)
  }

  def default(gateway: IP, source: Option[IP], link: Link[_]): Route = {
    DefaultRoute(gateway, source, link)
  }

}

object Routes {

  def add[F[_]](range: CIDR, gateway: Option[IP], source: Option[IP], link: Link[_])(implicit F: Sync[F]): Action[F, Route] = {
    add(Route.specific(range, gateway, source, link))
  }

  def add[F[_]](gateway: IP, source: Option[IP], link: Link[_])(implicit F: Sync[F]): Action[F, Route] = {
    add(Route.default(gateway, source, link))
  }

  def add[F[_]](route: Route)(implicit F: Sync[F]): Action[F, Route] = {
    val execute = route match {
      case SpecificRoute(range, gateway, source, link) =>
        val via = gateway.map(List("via", _)).getOrElse(List.empty[String])
        val src = gateway.map(List("src", _)).getOrElse(List.empty)
        Executor[F](sudo = Some(true), networkNamespace = link.namespace).execute(List("ip", "route", "add", range) ++ via ++ src ++ List("dev", link.name):_*).foreground.as(route)

      case DefaultRoute(gateway, source, link) =>
        val command = List("ip", "route", "add", "default", "via", gateway) ++ source.map(List("src", _)).getOrElse(List.empty[String]) ++ List("dev", link.name)
        Executor(sudo = Some(true), networkNamespace = link.namespace).execute(command: _*).foreground.as(route)
    }

    Action.make(execute)(delete(route))
  }

  def delete[F[_]](route: Route)(implicit F: Sync[F]): Action[F, Route] = {
    val execute = route match {
      case SpecificRoute(range, gateway, source, link) =>
        val via = gateway.map(List("via", _)).getOrElse(List.empty[String])
        val src = gateway.map(List("src", _)).getOrElse(List.empty)
        Executor(sudo = Some(true), networkNamespace = link.namespace).execute(List("ip", "route", "del", range) ++ via ++ src ++ List("dev", link.name):_*).foreground.as(route)

      case DefaultRoute(gateway, source, link) =>
        val command = List("ip", "route", "del", "default", "via", gateway) ++ source.map(List("src", _)).getOrElse(List.empty[String]) ++ List("dev", link.name)
        Executor(sudo = Some(true), networkNamespace = link.namespace).execute(command: _*).foreground.as(route)
    }

    Action.make(execute)(delete(route))
  }

}

class Routes(link: Link[_]) {

  def list[F[_]](implicit F: Sync[F]): Action[F, List[Route]] = {
    val regex = "^(default|[0-9\\.]+).*( via ([0-9\\.]*))?.*( src [0-9\\.]+))?".r
    val routes = for {
      stdout <- Executor[F](sudo = Some(true), networkNamespace = link.namespace).execute("ip", "route", "show", "dev", link.name).foreground[String](Keep.stdout)
      routes = stdout.split("\n").toList.collect({
        case regex("default", _, gateway, _, source) =>
          Route.default(gateway, Option(source), link)
        case regex(range, _, gateway, _, source) =>
          Route.specific(range, Option(gateway), Option(source), link)
      })
    } yield routes

    routes.action
  }

  def default[F[_]](implicit F: Sync[F]): Action[F, DefaultRoute] = {
    list[F]
      .flatMap(_.collectFirst({
        case route: DefaultRoute =>
          Action.pure(route)
      })
      .getOrElse(Action.raiseError[F, DefaultRoute](new Exception(s"Unable to find default route for ${link} link! "))))
  }

  def add[F[_]](range: CIDR, gateway: Option[IP], source: Option[IP])(implicit F: Sync[F]): Action[F, Route] = {
    Routes.add(Route.specific(range, gateway, source, link))
  }

  def add[F[_]](gateway: IP, source: Option[IP])(implicit F: Sync[F]): Action[F, Route] = {
    Routes.add(Route.default(gateway, source, link))
  }

}

