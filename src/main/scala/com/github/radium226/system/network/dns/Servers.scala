package com.github.radium226.system.network.dns

import cats.implicits._
import cats.effect._

import com.github.radium226.system.execute._
import com.github.radium226.system.action._

import com.github.radium226.system.network._
import com.github.radium226.system.network.namespace._


class Servers(namespace: Option[Namespace]) {

  @deprecated("It should work better because it's not okay... ")
  def add[F[_]](ip: IP)(implicit F: Sync[F]): Action[F, Unit] = {
    val execute: F[Unit] = namespace match {
      case None =>
        F.raiseError(new Exception("Unsupported :("))

      case Some(Namespace(namespaceName)) =>
        for {
          _ <- Executor(sudo = Some(true)).execute("mkdir", "-p", s"/etc/netns/${namespaceName}").foreground
          _ <- Executor(sudo = Some(true)).execute("sh", "-c", s"echo 'nameserver ${ip}' >'/etc/netns/${namespaceName}/resolv.conf'").foreground
        } yield ()
    }

    execute.action
  }

}

case object Servers extends Servers(None)
