package com.github.radium226.system.network.link

import cats.effect._

import com.github.radium226.system.network._
import com.github.radium226.system.action._
import com.github.radium226.system.execute._

class Addresses(link: Link[_]) {

  def add[F[_]](range: CIDR)(implicit F: Sync[F]): Action[F, Unit] = {
    val execute = Executor(sudo = Some(true), networkNamespace = link.namespace)
    .execute("ip", "address", "add", range, "dev", link.name)
    .foreground[Unit](Keep.unit)
    Action.make(execute)(delete(range))
  }

  def delete[F[_]](range: CIDR)(implicit F: Sync[F]): Action[F, Unit] = {
    val execute = Executor(sudo = Some(true), networkNamespace = link.namespace)
      .execute("ip", "address", "del", range, "dev", link.name)
      .foreground[Unit](Keep.unit)
    Action.make(execute)(add(range))
  }

}