package com.github.radium226

import java.nio.file.Path
import cats.implicits._

package object fs {

  /*trait FileSystem[F[_]] {
    self =>

    def listFilesAndFolders(folderPath: Path): F[List[Path]]

    def listFiles(folderPath: Path): F[List[Path]]

    def listFolders(folderPath: Path): F[List[Path]]

    def touchFile(filePath: Path): F[Unit]

    def createFolder(folderPath: Path): F[Unit]

    def deleteFolder(folderPath: Path): F[Unit]

    def fileExists(filePath: Path): F[Boolean]

    def folderExists(folderPath: Path): F[Boolean]

    def createSymbolicLink(): F[Unit]

  }*/

  sealed trait Kind

  object Kind {

    case object File extends Kind

    case object Folder extends Kind

    case object Link extends Kind

  }

}
