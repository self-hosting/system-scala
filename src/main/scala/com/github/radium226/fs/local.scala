package com.github.radium226.fs

import java.nio.file._
import java.nio.file.attribute.FileTime
import java.time.Instant

import cats.effect._
import cats.implicits._

import scala.jdk.CollectionConverters._
//import com.github.radium226.fs.FileSystem

//import com.google.


class LocalFileSystem[F[_]](blocker: Blocker)(implicit F: Sync[F], contextShift: ContextShift[F]) { //extends FileSystem[F] {

  def list(folderPath: Path): F[List[Path]] = {
    blocker.blockOn(F.delay(Files.walk(folderPath).iterator().asScala.toList))
  }

  def isFolder(fileOrFolderPath: Path): F[Boolean] = {
    blocker.blockOn(F.delay(Files.isDirectory(fileOrFolderPath)))
  }

  def isFile(fileOrFolderPath: Path): F[Boolean] = {
    blocker.blockOn(F.delay(Files.isRegularFile(fileOrFolderPath)))
  }

  def listFolders(folderPath: Path): F[List[Path]] = {
    for {
      filesAndFolders <- list(folderPath)
      folders         <- filesAndFolders.filterA(isFolder(_))
    } yield folders
  }

  def listFiles(folderPath: Path): F[List[Path]] = {
    for {
      filesAndFolders <- list(folderPath)
      files           <- filesAndFolders.filterA(isFile(_))
    } yield files
  }

  def touchFile(filePath: Path): F[Unit] = {
    blocker.blockOn(F.delay(
      if (Files.exists(filePath)) {
        Files.setLastModifiedTime(filePath, FileTime.from(Instant.now()))
      } else {
        Files.createFile(filePath)
      }
    ))
  }

  def createFolder(folderPath: Path): F[Unit] = {
    blocker.blockOn(F.delay(Files.createDirectories(folderPath)))
  }

  def deleteFolder(folderPath: Path): F[Unit] = {
    ??? //blocker.blockOn(F.delay(MoreFiles.delete()))
  }

  def exists(fileOrFolderPath: Path): F[Boolean] = {
    blocker.blockOn(F.delay(Files.exists(fileOrFolderPath)))
  }

  def createLink(): F[Unit] = ???
}

object LocalFileSystem {

  def apply[F[_]](blocker: Blocker)(implicit F: Sync[F], contextShift: ContextShift[F]): LocalFileSystem[F] = new LocalFileSystem[F](blocker)

}
