package com.github.radium226

import java.nio.file.{Files, Path}
import java.util.stream.Collectors

import cats._
import cats.data._
import cats.implicits._
import cats.effect._

import scala.collection.JavaConverters._

object FileSystem {

  def listFolders[F[_]](folderPath: Path)(implicit F: Sync[F]): F[List[Path]] = F.delay {
    Files.list(folderPath).collect(Collectors.toList()).asScala.toList
  }

}
